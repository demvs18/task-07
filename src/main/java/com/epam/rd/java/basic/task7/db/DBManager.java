package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringJoiner;

public class DBManager {

    public Connection connection;

    public static final String URL_DATA_BASE_CONNECTION = "connection.url";
    public static final String DATA_BASE_USER = "username";
    public static final String DATA_BASE_PASSWORD = "password";
    public static final String FILE_INPUT = "app.properties";

    public static final String FIND_ALL_USERS = "SELECT * FROM users ";
    public static final String FIND_ALL_TEAMS = "SELECT * FROM teams ";
    public static final String FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login LIKE ?";
    public static final String FIND_TEAM_BY_NAME = "SELECT * FROM teams WHERE name LIKE ?";
    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
        Properties declaration = loadProperties();
        String URL = declaration.getProperty(URL_DATA_BASE_CONNECTION);
        String USER = declaration.getProperty(DATA_BASE_USER);
        String PASSWORD = declaration.getProperty(DATA_BASE_PASSWORD);
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException dataBaseAccessesError) {
            throw new RuntimeException(dataBaseAccessesError);
        }
    }

    private static Properties loadProperties() {
        Properties characteristicDataBaseAccess = new Properties();
        try (InputStream inputFilePropertyConnection = new FileInputStream(FILE_INPUT)) {
            characteristicDataBaseAccess.load(inputFilePropertyConnection);
        } catch (IOException failedOInputOutputOperations) {
            throw new UncheckedIOException(failedOInputOutputOperations);
        }
        return characteristicDataBaseAccess;
    }

    public List<User> findAllUsers() throws DBException {
        try (Statement assertion = connection.createStatement()) {
            List<User> users = new ArrayList<>();
            User user;
            ResultSet resultSet = assertion.executeQuery(FIND_ALL_USERS);
            while (resultSet.next()) {
                user = new User(resultSet.getInt("id"), resultSet.getString("login"));
                users.add(user);
            }
            return users;
        } catch (SQLException dataBaseAccessError) {
            throw new DBException("Find all users is failed", dataBaseAccessError);
        }
    }

    public boolean insertUser(User user) throws DBException {
        boolean result = false;
        if (user != null) {
            try (Statement assertion = connection.createStatement()) {
                if (1 == assertion.executeUpdate("INSERT INTO users (login) VALUES ('" + user.getLogin() + "')", Statement.RETURN_GENERATED_KEYS)) {
                    try (ResultSet resultSet = assertion.getGeneratedKeys()) {
                        resultSet.next();
                        user.setId(resultSet.getInt(1));
                        result = true;
                    }
                }
            } catch (SQLException dataBaseAccessError) {
                throw new DBException("Insert user is failed", dataBaseAccessError);
            }
        }
        return result;
    }

    public boolean deleteUsers(User... users) throws DBException {
        if (users.length == 0) {
            return false;
        }
        StringJoiner stringJoiner = new StringJoiner(",", "DELETE FROM users WHERE id IN (", ")");
        for (User user : users) {
            if (user != null) {
                stringJoiner.add(String.valueOf(user.getId()));
            }
        }
        try (Statement statement = connection.createStatement()) {
            return statement.executeUpdate(stringJoiner.toString()) > 0;
        } catch (SQLException dataBaseAccessError) {
            throw new DBException("Delete users are failed", dataBaseAccessError);
        }
    }

    public User getUser(String login) throws DBException {
        try (PreparedStatement assertion = connection.prepareStatement(FIND_USER_BY_LOGIN)) {
            User user = null;
            assertion.setString(1, login);
            ResultSet resultSet = assertion.executeQuery();
            while (resultSet.next()) {
                user = new User(resultSet.getInt("id"), resultSet.getString("login"));
            }
            return user;
        } catch (SQLException dataBaseAccessError) {
            throw new DBException("Get user by login is failed", dataBaseAccessError);
        }
    }

    public Team getTeam(String name) throws DBException {
        try (PreparedStatement assertion = connection.prepareStatement(FIND_TEAM_BY_NAME)) {
            Team team = null;
            assertion.setString(1, name);
            ResultSet resultSet = assertion.executeQuery();
            while (resultSet.next()) {
                team = new Team(resultSet.getInt("id"), resultSet.getString("name"));
            }
            return team;
        } catch (SQLException dataBaseAccessError) {
            throw new DBException("Get team by id is failed", dataBaseAccessError);
        }
    }

    public List<Team> findAllTeams() throws DBException {
        try (Statement assertion = connection.createStatement()) {
            List<Team> teams = new ArrayList<>();
            Team team;
            ResultSet resultSet = assertion.executeQuery(FIND_ALL_TEAMS);
            while (resultSet.next()) {
                team = new Team(resultSet.getInt("id"), resultSet.getString("name"));
                teams.add(team);
            }
            return teams;
        } catch (SQLException dataBaseAccessError) {
            throw new DBException("Find all users is failed", dataBaseAccessError);
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        boolean result = false;
        if (team != null) {
            try (Statement assertion = connection.createStatement()) {
                if (1 == assertion.executeUpdate("INSERT INTO teams (name) VALUES ('" + team.getName() + "')", Statement.RETURN_GENERATED_KEYS)) {
                    try (ResultSet resultSet = assertion.getGeneratedKeys()) {
                        resultSet.next();
                        team.setId(resultSet.getInt(1));
                        result = true;
                    }
                }
            } catch (SQLException dataBaseAccessError) {
                throw new DBException("Insert team is failed", dataBaseAccessError);
            }
        }
        return result;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        if (user == null || teams == null) {
            return false;
        }
        try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users_teams (user_id, team_id) VALUES (" + user.getId() + ",?)");) {
            connection.setAutoCommit(false);
            boolean isInserted = false;
            for (Team team : teams) {
                if (team != null) {
                    preparedStatement.setInt(1, team.getId());
                    isInserted = (preparedStatement.executeUpdate() > 0) || isInserted;
                }
            }
            connection.commit();
            connection.setAutoCommit(true);
            return true;
        } catch (SQLException dataBaseAccessError) {
            tryRollBack(connection);
            throw new DBException("Set Teams for User failed", dataBaseAccessError);
        }
    }

    private void tryRollBack(Connection connection) {
        if (connection != null) {
            try {
                connection.rollback();
            } catch (SQLException dataBaseAccessError) {
                dataBaseAccessError.printStackTrace();
            }
        }
    }

    private void tryClose(AutoCloseable con) {
        if (con != null) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();

        try (ResultSet rs = connection.createStatement().executeQuery("SELECT team_id, name FROM users_teams JOIN teams ON team_id = id WHERE user_id = " + user.getId())) {

            while (rs.next()) {
                Team team = Team.createTeam(rs.getString("name"));
                team.setId(rs.getInt("team_id"));
                teams.add(team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getUserTeams() failed", e);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        if (team == null) {
            return false;
        }
        try (Statement statement = connection.createStatement()) {
            return statement.executeUpdate("DELETE FROM teams WHERE name = '" + team.getName() + "'") > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("deleteTeam() failed", e);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        if (team == null) {
            return false;
        }
        try (Statement assertion = connection.createStatement()) {
            return assertion.executeUpdate("UPDATE teams SET name = '" + team.getName() + "' WHERE id = " + team.getId()) > 0;
        } catch (SQLException dataBaseAccessError) {
            throw new DBException("Find all users is failed", dataBaseAccessError);
        }
    }
}
